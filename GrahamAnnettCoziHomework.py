# graham annett

import sys
import re


TEST_EXAMPLE = """!name1=John Q.
!name2=Smith
!salutation=Dear @name1 @name2
1 Infinite Loop
!product=Horcrux Widget
Somewheresville, CA 98765


@salutation,


Thank you for your interest in @{product}s.
Unfortunately, we sold our last @product yesterday.


@name1, if you have any more questions about our products,
email us at support@@horcrux.com, tweet to @@horcrux_support,
or call us @@ 1-800-HORCRUX."""


def get_data():
    '''read data from stdin if not testing'''
    input_data = sys.stdin.read()
    if len(input_data) == 0:
        print('No Input Data')
        sys.exit()
    return input_data


def fix_vars(variables):
    '''replace placeholder variables'''
    for key, v in variables.items():
        replaced_value = ' '.join([variables.get(val, val) for val in v.split()])
        if replaced_value != v:
            variables[key] = replaced_value


def parse_input(input_data):
    '''create output from variables/parsed input'''
    input_data = input_data.split('\n')
    variables = {}

    output = ''
    for idx, line in enumerate(input_data):
        if line is '':
            break
        if line[0] != '!':
            output += '\n{line}'.format(line=line)
        else:
            match = re.search(r'(?<=\!)(.*?)=(?=)(.*)', line)
            variables['@' + match.groups()[0]] = match.groups()[1]
    fix_vars(variables)

    # transform output using dict/replace weird format things
    for line in input_data[idx:]:
        for key in variables.keys():
            if key in line:
                line = line.replace(key, variables[key])

        # match the random curly braces aspect
        match = re.search(r'(\@)(\{)(?=)(.*)(\})', line)
        if match and (match.groups()[0] + match.groups()[2]) in variables:
            saved_variable = variables[match.groups()[0] + match.groups()[2]]
            matched_var = ''.join(match.groups())
            if matched_var in line:
                line = line.replace(matched_var, saved_variable)

        # replace @@ last since im not entirely sure if it's supposed to entail 
        # something else
        line = line.replace('@@', '@')

        output += '\n' + line
    return output


def generate_output(unformatted_output, variables):
    reut


def main(test=False):
    if test:
        data = TEST_EXAMPLE
    else:
        data = get_data()
    print(parse_input(data))


if __name__ == '__main__':
    main()
