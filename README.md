# info for assignment


hey

<!-- ```
!name1=John Q.

!name2=Smith

!salutation=Dear @name1 @name2

1 Infinite Loop

!product=Horcrux Widget

Somewheresville, CA 98765

@salutation,

Thank you for your interest in @{product}s.

Unfortunately, we sold our last @product yesterday.

@name1, if you have any more questions about our products,

email us at support@@horcrux.com, tweet to @@horcrux_support,

or call us @@ 1-800-HORCRUX.
``` -->





```
!name1=John Q.
!name2=Smith
!salutation=Dear @name1 @name2
1 Infinite Loop
!product=Horcrux Widget
Somewheresville, CA 98765

 
@salutation,

 
Thank you for your interest in @{product}s.
Unfortunately, we sold our last @product yesterday.

 
@name1, if you have any more questions about our products,
email us at support@@horcrux.com, tweet to @@horcrux_support,
or call us @@ 1-800-HORCRUX.
```



## Your solution should generate the following output:

```
1 Infinite Loop
Somewheresville, CA 98765

 
Dear John Q. Smith,

 
Thank you for your interest in Horcrux Widgets.
Unfortunately, we sold our last Horcrux Widget yesterday.

 
John Q., if you have any more questions about our products,
email us at support@horcrux.com, tweet to @horcrux_support,
or call us @ 1-800-HORCRUX.
```
 
Your solution should be one of the following:
A filter that reads from stdin and writes to stdout.
JavaScript in an HTML page. Your code should respond to a Process button click, by reading input from a textarea and writing the output to a pre block.
A GUI application with a text input region, a Process button, and an output region. Bonus points if there's a Browse button that opens a file and places its contents into the text input region.

 
Note:
You may assume that the input is 7-bit ASCII.
Do not assume that the input will “compile” correctly.
There may be an arbitrary number of !-assignment directives interspersed throughout the input.
The color in the sample input and output is only to make the samples easier to read.
 
Please send us a zipfile containing a Windows, Mac, or Linux executable; a README with brief installation and build instructions and other notes; your source code; and your tests. (For scripting languages like Python or Ruby, the source will suffice.)
If your email system won't let you send a zipfile, upload it to Dropbox.com or a similar service.
